/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

function userInfo(){
	let fullName = prompt("Enter your full name: ");
	let age = prompt("Enter your age: ");
	let location = prompt("Enter your location: ");
	alert("Thank you "+fullName+" for your input!");
	console.log("Hello, "+fullName+"!");
	console.log("You are "+age+" years old");
	console.log("You live in "+location);
}

userInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

function favoriteMusic(){
	console.log("1. Holopro");
	console.log("2. Parokya ni Edgar");
	console.log("3. Ado");
	console.log("4. Masayoshi Soken");
	console.log("5. Shoji Meguro");
}

console.log	("These are my favorite musical talents: ");
favoriteMusic();



/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

function favoriteMovies(){
	console.log("1. Shrek");
	console.log("Rotten Tomatoes Rating: 88%");
	console.log("2. Puss in Boots: The Last Wish");
	console.log("Rotten Tomatoes Rating: 95%");
	console.log("3. Spider-man: No Way Home");
	console.log("Rotten Tomatoes Rating: 93%");
	console.log("4. Lord of the Rings: Two Towers");
	console.log("Rotten Tomatoes Rating: 95%");
	console.log("5. Lord of the Rings: Return of the King");
	console.log("Rotten Tomatoes Rating: 93%");
	
}

console.log	("These are my favorite movies along with their Rotten Tomatoes rating: ");
favoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
// console.log(friend1);
// console.log(friend2);
